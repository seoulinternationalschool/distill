import sys, os, urwid
from termcolor import colored, cprint
from ui import TUI
from cli import DiscordCLI

#Config
try:
  import config
except ImportError: 
  cprint('Please copy example_config.py to config.py', 'red')
  exit()

#Arguments
if (len(sys.argv) == 1):
  cprint('Please specify mode: TUI, CLI', 'red')
  exit()

def initcli():
  print ('Init CLI')
  cli = DiscordCLI(config.USER)
  cli.cmdloop()

#Run
def inittui():
  if (sys.argv[1] == 'TUI'):
    with term.fullscreen():
      tui = TUI(config.TUI)
      tui.textchannels.addoption('lmao')
      tui.voicechannels.addoption('ffs')
      tui.guilds.addoption('lmao')
      tui.guilds.addoption('ffs')
      #tui.content.append(tui.guilds.output())   
      tui.horizontal_boxes.openbox(tui.guilds, 'given', 24)
      tui.update()


if(sys.argv[1] == 'CLI'):
  initcli()

if(sys.argv[1] == 'TUI'):
  initcli()
  inittui()

