import cmd, sys, argparse
from termcolor import cprint
from . import args_parse, text_shell


class DiscordCLI(cmd.Cmd):
  def __init__(self, app, **kwargs):
    super(DiscordCLI, self).__init__(**kwargs)
    self.app = app

  intro = 'This is the CLI interface for the linux DISCORD utility. \n\n' + 'Please enter a command to continue.' 
  prompt = '[discord-tui]: '
  file = None

  #== Before the command loop runs ==#
  def preloop(self):

    # get all the guilds
    self.guild_list = self.app.guilds.get_all()
    self.dm_list = self.app.dm.get_all()


    pass
    
  #== Commands ==#
  def do_get(self, arg):

    args = self.get_parser.parse_args(arg.split())

    want = getattr(args, 'type')

    if want == 'server':
      servs = self.guild_list
      
      serv_ind = 0

      for i in servs:
        cprint('{0:<30}'.format(str(serv_ind) + ':'), 'white')
        cprint('{0:<30}'.format(i['name']), 'white')
        cprint('{0:<30}'.format(i['id']), 'white')
        
        serv_ind += 1
        print('\n')

    elif want == 'dm':
      dms = self.dm_list

      dm_ind = 0

      for i in dms:
        i=i.details
        cprint('{0:<5}{1:<30}'.format(str(dm_ind) + ':', i['recipients'][0]['username']), 'blue')
        cprint('     {0:<30}'.format(i['id']), 'white')
        print('\n')
        dm_ind += 1
  

  def do_expand(self, arg):

    args = self.expand_parser.parse_args(arg.split())

    want = getattr(args, 'type') 

    if want == 'server':

      by_index = getattr(args, 'by_index')
      by_name  = getattr(args, 'by_name')

      if by_index != None:
        server = self.guild_list[by_index]
      elif by_name != None:
        try:
          server = [x for x in self.guild_list if x['name'] == by_name][0]
        except:
          print('no such server was found!')
      else:
        print('no name was provided bruh')

      self.expand_print(server, 'server')

    

  def do_opchan(self, arg):

    args = self.opchan_parser.parse_args(arg.split())

    want = getattr(args, 'type')
    by_index = getattr(args, 'by_index')
    by_name  = getattr(args, 'by_name')

    if want == 'text_guild':
      if by_index != None:
        self._open_text_shell([x for x in self.channels_list if x.details['position'] == by_index][0])
      elif by_name != None:
        self._open_text_shell([x for x in self.channels_list if x.details['name'] == by_name][0])
      else:
        print('bruv no')

    elif want == 'text_dm':
      if by_index != None:
        self._open_text_shell(self.dm_list[by_index])
      elif by_name != None:
        self._open_text_shell([x for x in self.channels_list if x.details['name'] == by_name][0])
      else:
        print('bruv no')

  #== External functions, attributes ==#

  get_parser = args_parse._init_parser_get()
  expand_parser = args_parse._init_parser_expand()
  opchan_parser = args_parse._init_parser_opchan()

  def expand_print(self, disp_obj, disp_type):
    if disp_type == 'server':
      server_id = disp_obj['id']

      guild = self.app.guilds.get(server_id)
      channels = guild.channels.get_all()
      self.channels_list = channels

      cprint('{0:<30}'.format(guild.details['name']), 'white')
      cprint('{0:<30}'.format(guild.details['id']), 'white')
      cprint('{0:<30}'.format(guild.details['region']), 'white')

      for i in range(len(channels) - 2):
        try:
          channel = [x.details for x in channels if x.details['position'] == i][0]
          cprint('{0:<30}'.format(channel['name']), 'white')
          cprint('{0:<30}'.format(channel['id']), 'white')
          cprint('{0:<30}'.format(channel['position']), 'white')
        except:
          pass
      


    elif disp_type == 'dm':
      pass
      
  

  def _open_text_shell(self, tchannel):
    shell = text_shell.TextChannelShell(self.app, tchannel)

    if 'name' in tchannel.details:
      name = tchannel.details['name']
    else:
      name = tchannel.details['recipients'][0]['username']

    shell.prompt = self.prompt + 'TEXT_CHANNEL [{}]: '.format(name)
    shell.cmdloop()



