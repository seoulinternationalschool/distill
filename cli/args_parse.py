import argparse

def _init_parser_get():
  parser = argparse.ArgumentParser(description= 'this is a parser for the CLI get command')
  
  parser.add_argument('type', action='store')
  parser.add_argument('-l', '--long', action='store_true', dest='long')

  return parser

def _init_parser_expand():
  parser = argparse.ArgumentParser(description= 'this is a parser for the CLI')
  
  parser.add_argument('type', action='store')
  parser.add_argument('-i', '--index', action='store', dest='by_index', type=int)
  parser.add_argument('-n', '--name', action='store', dest='by_name', type=str)

  return parser

def _init_parser_opchan():
  parser = argparse.ArgumentParser(description= 'this is a parser for the CLI')
  
  parser.add_argument('type', action='store')
  parser.add_argument('-i', '--index', action='store', dest='by_index', type=int)
  parser.add_argument('-n', '--name', action='store', dest='by_name', type=str)

  return parser