import cmd, json
from termcolor import cprint

class TextChannelShell(cmd.Cmd):
  def __init__(self, app, channel, **kwargs):
    super(TextChannelShell, self).__init__(**kwargs)
    self.app = app
    self.channel = channel


    self.app.gateway.define_task('message_create', self._in_shell)

  intro = 'You are now in a text channel'


  def preloop(self):
    pass

  def default(self, arg):


      if(arg == '--exit'):
        return True

      content = {
        'content': arg
      }
      self.channel.send_message(content)
      print('\n')


  def _in_shell(self, msg):
 
    if msg['channel_id'] == self.channel.details['id']:
      if msg['author']['id'] == self.app.profile._id:
        return 
      else:
        # print the message to the line above!!
        cprint('{}: {}\n'.format(msg['author']['username'], msg['content']), 'green')
    