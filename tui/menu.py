import urwid

class Menu(urwid.ListBox):
  def __init__(self, name, callback):
    super(Menu, self).__init__([])
    self.callback = callback
    self.name = name
    self.options = []
    self._listwalker = urwid.SimpleFocusListWalker([])

  def addoption(self, name):
    menu_button = MenuButton(name, self.callback)
    self.options.append(menu_button)
    self.update()
  
  def clearoptions(self):
    del self.options[:]

  def update(self):
    self._listwalker = [
      urwid.Text(self.name), 
      urwid.Divider()]
    self._listwalker.extend(self.options)
    self.body = self._listwalker

class MenuButton(urwid.Button):
  def __init__(self, caption, callback):
    super(MenuButton, self).__init__("")
    self.caption = caption
    urwid.connect_signal(self, 'click', callback)
    self.unselect()

  def unselect(self):
    self.sel_icon = urwid.SelectableIcon([self.caption], 1)
    self._w = urwid.AttrMap(self.sel_icon, 'button', 'button_focused')
  def select(self):
    self.sel_icon = urwid.SelectableIcon([self.caption], 1)
    self._w = urwid.AttrMap(self.sel_icon, 'button_selected', 'button_selected_focused')

  
