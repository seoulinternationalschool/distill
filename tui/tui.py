import urwid
from .menu import Menu
from .horizontal_boxes import HorizontalBoxes
from .vertical_boxes import VerticalBoxes

class TUI:
  def __init__(self, config):
    self.current_guild = 0
    self.config = config
    self.current_textchannel = 0
    self.current_voicechannel = 0
    
    self.guilds = Menu('guilds', self.guild_click)
    self.channels = VerticalBoxes()
    self.textchannels = Menu('textchannels', self.textchannel_click)
    self.voicechannels = Menu('voicechannels', self.voicechannel_click)
    self.textchat = Menu('chat', self.textchat_click)
    self.users = Menu('users', None)
    self.horizontal_boxes = HorizontalBoxes() 
    self.text_input = urwid.Edit()
  
  def include_channels(self):
    self.channels.closeboxes()
    self.channels.openbox(self.textchannels, 'weight', 10)
    self.channels.openbox(self.voicechannels, 'weight', 10)

  def guild_click(self, button):
    if (self.current_guild):
      self.current_guild.unselect()
    self.current_guild = button
    self.current_guild.select()

    self.horizontal_boxes.closeboxes()
    self.horizontal_boxes.openbox(self.guilds, 'given', 24)
    self.horizontal_boxes.openbox(self.channels, 'given',24)
    # self.horizontal_boxes.op
    self.update()
  
  def channel_click(self, button):
    self.horizontal_boxes.closeboxes()
    self.horizontal_boxes.openbox(self.channels, 'given',24)
    self.update() 
  def textchannel_click(self, button):
    self.channel_click(button)
    # TODO
  def voicechannel_click(self, button):
    self.channel_click(button)
    # TODO

  def textchat_click(self, button):
    return 
    #TODO

  def sel_channel(self):
    return 0

  def sel_guild(self):
    return 0

  def textchannel(self):
    return 0
        
  def update(self):
    self.include_channels()
    self.main_box = urwid.Frame(self.horizontal_boxes, footer=self.text_input)
    self.main_loop = urwid.MainLoop(self.main_box, self.config['palette'])
    urwid.escape.SHOW_CURSOR = ''
    self.main_loop.run()

