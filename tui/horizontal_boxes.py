import urwid

class HorizontalBoxes(urwid.Columns):
  def __init__(self):
    super(HorizontalBoxes, self).__init__([], dividechars = 0)
  
  def openbox(self, box, options, width):
    tup = (box, self.options(options, width)) 
    self.contents.append(tup)
    self.focus_position = len(self.contents) - 1
  
  def closeboxes(self):
    del self.contents[:]
