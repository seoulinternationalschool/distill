import urwid

class VerticalBoxes(urwid.Pile):
  def __init__(self):
    super(VerticalBoxes, self).__init__([])
  
  def openbox(self, box, options, width):
    tup = (box, self.options(options, width)) 
    self.contents.append(tup)
    self.focus_position = len(self.contents) - 1
  
  def closeboxes(self):
    del self.contents[:]
