import os, tracemalloc, time
import psutil
from subprocess import Popen

tracemalloc.start()

def go():
  pipe_name = '/tmp/2_pipe'

  if not os.path.exists(pipe_name):
    os.mkfifo(pipe_name)

  Popen(['xterm', '-e', 'tail -f {}'.format(pipe_name)])  

  before = tracemalloc.take_snapshot()

  while True:
    snap = tracemalloc.take_snapshot()
    stat = snap.compare_to(before,'lineno')
    before = snap
    stat = [r for r in stat if 'malloc' not in str(getattr(r, 'traceback'))]
    with open(pipe_name, 'w') as w:
      w.write("\033[0;0H")
      for t in stat[:10]:
        w.write('\033[K')

        w.write('{:<7.2f} KB {:<8.2f}'.format((t.size / 1024), (t.size_diff / 1024)))
        w.write(' ' + str(t.traceback).split('/')[-1])
        w.write('\n')
      w.write('{:<10.4f} KB\n'.format(sum(getattr(d, 'size') for d in stat)/1024))
      w.write('{:<5}% CPU'.format(psutil.cpu_percent()))
      time.sleep(1)


 
