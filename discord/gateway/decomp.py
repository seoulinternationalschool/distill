import zlib

def decode_message(msg):

  ZLIB_SUFFIX = '\x00\x00\xff\xff'
  buffer_cache = bytearray()

  inflator = zlib.decompressobj()

  buffer_cache.extend(msg)

  if len(msg) < 4 or msg[-4:] != ZLIB_SUFFIX:
    return
  

  msg = inflator.decompress(buffer_cache).decode('utf-8')
  return msg

