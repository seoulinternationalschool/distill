import websockets, asyncio, ssl, json
from .opcodes import * 
from datetime import datetime
from .decomp import decode_message
from ..voice.conn import DiscordVoiceSocket
from time import sleep

class GatewayUtil:
  def __init__(self, app):
    self.app = app
    self.tasks = {}
    self.loop = asyncio.get_event_loop()

  async def dispatch_task(self, event, information):

    try:
      for fn in self.tasks[event]:
        fn(information)
    except:
      pass

  async def meta_task(self, infor):
    op_code = infor['op']
    main_inf = infor['d']

    if op_code == 1:
      self._send_heartbeat()

    #elif op_code == 7:
      #self._resume()

    #elif op_code == 9:
      #self._invalid_session_err()

    elif op_code == 10:

      self._heartbeat_interval = main_inf['heartbeat_interval'] / 1000
      await self._send_heartbeat()
      await self._identify()
      asyncio.ensure_future(self.heart_beater(), loop=self.loop)

    elif op_code == 11:
      self._ack = datetime.now().time()

  def define_task(self, event, function):
    if event not in self.tasks:
      self.tasks[event] = []

    self.tasks[event].append(function)

  def clear_tasks(self, *events):
    try:
      for event in events:
        self.tasks[event] = []
    except:
      print('tasklist was not inited before')

  async def connect(self):
    ws_url = self.app.http.request('get', '/gateway')
    ws_url = ws_url['url'] + '/?v=6&encoding=json'
    self.ws_connection = await websockets.client.connect(ws_url, ssl=ssl.SSLContext())
  
    async for message in self.ws_connection:
      message = json.loads(message)

      if message['op'] == 0:

        await self.dispatch_task(message['t'].lower(), message['d'])
      else:
        await self.meta_task(message)

  def start(self):
    self.app.thread_pool.add_thread(self.start_loop)

  def start_loop(self):
    self.loop.run_until_complete(self.connect())
    self.loop.close()

  async def heart_beater(self):
    while True:

      await self._send_heartbeat()
      await asyncio.sleep(self._heartbeat_interval)

  async def change_voice_state(self, guild_id, channel_id):
    while not hasattr(self, 'ws_connection'):
      await asyncio.sleep(0.7)

      continue

    await self.ws_connection.send(opcode_g.FOUR(guild_id, channel_id))
    self.vsw = self.VoiceStateWaiter(self, guild_id)

    self.define_task('voice_state_update', self.vsw.add_state)
    self.define_task('voice_server_update', self.vsw.add_serv)

  async def _send_heartbeat(self):
    await self.ws_connection.send(opcode_g.ONE(None))
  
  async def _identify(self):

    await self.ws_connection.send(opcode_g.TWO(self.app.token))

  class VoiceStateWaiter:
    def __init__(self, gate, serv):
      self.gate = gate
      self.got_state = False
      self.got_serv = False
      self.sess_id = 0
      self.endpoint = ''
      self.serv_id = serv

    #add voice state data / session_id, then check if the other one is loaded
    def add_state(self, pload):
      self.sess_id = pload['session_id']

      #return if wrong payload
      if pload['user_id'] != self.gate.app.profile._id:
        return

      self.got_state = True
      if self.got_serv:
        self.voice_init(self.sess_id, self.endpoint)
        self.terminate()

    # add server update data, check if other one was loaded
    def add_serv(self, pload):
      self.endpoint = pload['endpoint']
      self.token = pload['token']
      self.got_serv = True

      if self.got_state:
        self.voice_init(self.sess_id, self.endpoint)
        self.terminate()

    def voice_init(self, sid, end):
        self.gate.app.voice = DiscordVoiceSocket(self.gate.app,
                                                 self.endpoint,
                                                 server_id=self.serv_id,
                                                 session_id=self.sess_id,
                                                 user_id=self.gate.app.profile._id,
                                                 token=self.token)
        self.gate.app.voice.start()
        print(self.sess_id + '@' + self.endpoint)

    def terminate(self):

      # clear hooks
      self.gate.clear_tasks('voice_state_update', 'voice_server_update')

      # clear attribute so we can reset again
      delattr(self.gate, 'vsw')