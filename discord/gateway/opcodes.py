import json, os

class opcode_g:
  
  @staticmethod
  def ONE(d_val):
    if not d_val:
      d_val = 'null'

    json_d = {
      'op': 1,
      'd': d_val
    }

    return json.dumps(json_d)

  @staticmethod
  def TWO(token):
    json_d = {
      "token": token,
      "properties": {
        "$os": os.name,
        "$browser": "tui",
        "$device": "tui"
      }
    }

    json_d = {
      'op': 2,
      'd': json_d
    }

    return json.dumps(json_d)

  @staticmethod
  def FOUR(gid, cid):
    return json.dumps({
      'op': 4,
      'd': {
        'guild_id': gid,
        'channel_id': cid,
        'self_mute': False,
        'self_deaf': False
      }
    })



