class terc:
  black = '\033[0;30m'
  red = '\033[0;31m'
  green = '\033[0;32m'
  orange = '\033[0;33m'
  blue = '\033[0;34m'
  purple = '\033[0;35m'
  cyan = '\033[0;36m'
  gray = '\033[0;37m'
  dgray = '\033[1;30m'
  lred = '\033[1;31m'
  lgreen = '\033[1;32m'
  yellow = '\033[1;33m'
  lblue = '\033[1;34m'
  lpurple = '\033[1;35m'
  lcyan = '\033[1;36m'
  white= '\033[1;37m'
  none = '\033[0m'

  #== color in text ==#
  def colorin(self, string, color):
    return getattr(self, color) + string + self.none