import requests
from .data import constants



class Requester:
  def __init__(self, app):
    self.app = app
    
  def path(self, url):
    return (constants.BASE_URL + url)

  # if authorized=True, the reference App token will be used. 
  def request(self, meth, route, authorized = False, **query):

    # if True, add auth header
    if authorized == True:
      query['headers'] = {
        'Authorization': self.app.token
      }
    # if no params is passes, make an empty dict
    if 'params' not in query.keys():
      query['params'] = {}

      # basic params needed in all calls, in x-www-urlendcoded
      query['params']['client_id'] = self.app.config['cid']
      query['params']['client_secret'] = self.app.config['cs']
      query['params']['grant_type'] = 'authorization_code'
    
    # make request with method, path, and query
    r = getattr(requests, meth)(self.path(route), **query)
    return r.json()

  # wrapper for convinience
  def api_login(self, id, passwd):
    self.payload = {
      'email': id,
      'password': passwd
    }
    return (self.request('post', constants.ROUTES['LOGIN'], json=self.payload))

  #wrapper for convinience, TODO: remove if not needed
  def api_refresh(self):
    return (self.request('post', constants.ROUTES['LOGIN'], json=self.payload))