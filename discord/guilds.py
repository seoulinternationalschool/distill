from .data import constants
from .channels import TextChannels

class GuildUtil:
    def __init__(self, app):
      self.app = app
      self.tchan = TextChannels(self.app)
    
    def get_all(self, limit = 50):
      payload = {
        'limit': limit
      }
      return (self.app.http.request('get', constants.ROUTES['MY_GUILDS'], True, params=payload))
    
    def leave(self, id):
      return (self.app.http.request('delete', constants.ROUTES['MY_GUILDS'] + "/" + id, True))

    def get(self, id):
      return self._g(self.app, self.app.http.request('get', constants.ROUTES['GUILDS'] % id, True), self.tchan)

    def create(self, id, d):
      return (self.app.http.request('post', constants.ROUTES['GUILDS'] % '', True, json=d))

    def modify(self, id, d):
      return (self.app.http.request('patch', constants.ROUTES['GUILDS'] % id, True, json=d))

    def delete(self, id, d):
      return (self.app.http.request('delete', constants.ROUTES['GUILDS'] % id, True, json=d))

    def join(self, id, uid, d):
      d['access_token'] = self.app.token
      return (self.app.http.request('delete', constants.ROUTES['GUILDS'] % id + '/members/' + uid, True, json=d))

    class _g:
      def __init__(self, app, guild, tchan):
        self.app = app
        self.id = guild['id']
        self.details = guild
        self._endpoint = constants.ROUTES['GUILDS'] % self.id
        self.tchan = tchan
        self.channels = self._c(self)
        self.users = self._u(self)
        self.util = self._ut(self)

      #channel utility for guilds
      class _c: 
        def __init__(self, guild):
          self.guild = guild

        def get_all(self):
          channels = self.guild.app.http.request('get', self.guild._endpoint + '/channels', True)
          return [self.guild.tchan.Channel(self.guild.app, x) for x in channels]

        def create(self, d):
          return self.guild.app.http.request('post', self.guild._endpoint + '/channels', True, json=d)

        def modify(self, d):
          return self.guild.app.http.request('patch', self.guild._endpoint + '/channels', True, json=d)

        def get(self, channel):
          return self.guild.tchan.get(channel)

      #user utility for guilds
      class _u:
        def __init__(self, guild):
          self.guild = guild

        def get(self, id):
          return self.guild.app.http.request('get', self.guild._endpoint + '/%s' % id, True)

        #TODO: apply config settings
        def get_all(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/members', True)
        
        def modify(self, id, d):
          return self.guild.app.http.request('patch', self.guild._endpoint + '/members/%s' % id, True, json=d)
        
        def modify_me(self, id, d):
          return self.guild.app.http.request('patch', self.guild._endpoint + '/members/@me/nick', True, json=d)

        def remove(self, id):
          return self.guild.app.http.request('delete', self.guild._endpoint + '/members/%s' % id, True)

        def add_role(self, id, role):
          return self.guild.app.http.request('put', self.guild._endpoint + '/members/%s/roles/%s' % (id, role), True)

        def remove_role(self, id, role):
          return self.guild.app.http.request('delete', self.guild._endpoint + '/members/%s/roles/%s' % (id, role), True)

      class _ut:
        def __init__(self, guild):
          self.guild = guild

        def get_bans(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/bans', True)
        
        def ban_user(self, id, d):
          return self.guild.app.http.request('put', self.guild._endpoint + '/bans/%s' % id, True, json=d)

        def unban_user(self, id):
          return self.guild.app.http.request('delete', self.guild._endpoint + '/bans/%s' % id, True)

        def get_roles(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/roles', True)

        def create_role(self, d):
         return self.guild.app.http.request('post', self.guild._endpoint + '/roles', True, json=d)   

        def modify_role_position(self, d):
         return self.guild.app.http.request('patch', self.guild._endpoint + '/roles', True, json=d) 

        def modify_role(self, role_id, d):
         return self.guild.app.http.request('patch', self.guild._endpoint + '/roles/%s' % role_id, True, json=d) 

        def delete_role(self, role_id):
         return self.guild.app.http.request('delete', self.guild._endpoint + '/roles/%s' % role_id, True) 

        def prune_limit(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/prune', True) 

        def prune(self, d):
          return self.guild.app.http.request('post', self.guild._endpoint + '/prune', True, json=d) 
        
        def voice_regions(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/regions', True) 
        
        def invites(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/invites', True) 

        def get_integrations(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/integrations', True) 

        def create_integration(self, d):
          return self.guild.app.http.request('post', self.guild._endpoint + '/integrations', True, json=d)

        def modify_integration(self, int_id, d):   
          return self.guild.app.http.request('get', self.guild._endpoint + '/integrations/%s' % int_id, True, json=d)

        def delete_integration(self, int_id):
          return self.guild.app.http.request('get', self.guild._endpoint + '/integrations/%s' % int_id, True)

        def sync_integration(self, int_id):
          return self.guild.app.http.request('post', self.guild._endpoint + '/integrations/%s/sync' % int_id, True)

        def get_embed(self):
          return self.guild.app.http.request('get', self.guild._endpoint + '/embed', True)

        def modify_embed(self, d):
          return self.guild.app.http.request('patch', self.guild._endpoint + '/embed', True)

        def get_vanity_url(self):
          return self.guild.app.http.request('post', self.guild._endpoint + '/vanity-url', True)