import json



def json_read(directory):
  return (json.loads(open(directory, 'r').read()))

#base url for api endpoint
BASE_URL = 'https://discordapp.com/api/v6'

#different routes for different functions
ROUTES = {
  'LOGIN': '/auth/login',
  'MY_PROFILE': '/users/@me',
  'GUILDS': '/guilds/%s',
  'MY_GUILDS': '/users/@me/guilds',
  'MY_DM': '/users/@me/channels',
  'CH_MSG': '/channels/%s/messages',
  'GUILD_CHANNELS': '/guilds/%s/channels'
}

# config for user, id, etc.
#TODO CONFIG = json_read('./config.json') 
