from collections import namedtuple

AlertString = namedtuple('Alert', 'if_err_type msg name')

HTTP = {
  '200': AlertString(if_err_type='only_ok', name='OK', msg='http.200: request was successful.'),

  '201': AlertString(if_err_type='only_ok', name='CREATED', msg='http.201: entity creation was successful.'),

  '204': AlertString(if_err_type='pass', name='NO_CONTENT', msg='http.204: request was successful, but no content was returned.'),

  '304': AlertString(if_err_type='pass', name='NOT_MODIFIED', msg='http.304: request was successful, but no modification took place.'),

  '400': AlertString(if_err_type='terminal', name='BAD_REQUEST', msg='http.400: request was unreadable to server.'),

  '401': AlertString(if_err_type='terminal', name='UNAUTHORIZED', msg='http.401: no valid Authorization request header.'),

  '403': AlertString(if_err_type='warn', name='FORBIDDEN', msg='http.403: Authorization header [token] header is not permitted to access resource.'),

  '404': AlertString(if_err_type='warn', name='NOT_FOUND', msg='http.404: resource was not found at location.'),

  '405': AlertString(if_err_type='terminal', name='METHOD_NOT_ALLOWED', msg='http.405: the request method is not allowed.'),

  '429': AlertString(if_err_type='warn', name='TOO_MANY_REQUESTS', msg='http.429: too many requests over rate limit.'),

  '502': AlertString(if_err_type='warn', name='GATEWAY_UNAVAILABLE', msg='http.502: no gateway was available to process request, try again.'),

  '5XX': AlertString(if_err_type='warn', name='SERVER_ERROR', msg='http.5**: there was a server error, try again. '),
}

GATEWAY = {
  '4000': AlertString(if_err_type='warn', name='UNKNOWN_ERR', msg='gateway.4000: something happened...try again.'),

  '4001': AlertString(if_err_type='terminal', name='OPCODE_UNKNOWN', msg='gateway.4001: invalid gateway opcode OR invalid payload for opcode.'),

  '4002': AlertString(if_err_type='terminal', name='DECODE_ERROR', msg='gateway.4002: invalid payload encode'),

  '4003': AlertString(if_err_type='warn', name='NO_AUTH', msg='gateway.4003: no identify payload was sent'),

  '4004': AlertString(if_err_type='terminal', name='INVALID_AUTH', msg='gateway.4004: identifying token was invalid'),

  '4005': AlertString(if_err_type='warn', name='ALREADY_AUTH', msg='gateway.4005: redundant identification took place'),

  '4007': AlertString(if_err_type='terminal', name='INVALID_SEQ', msg='gateway.4007: bad sequence for resume'),

  '4008': AlertString(if_err_type='warn', name='RATE', msg='gateway.4008: too many requests were made'),

  '4009': AlertString(if_err_type='terminal', name='SESS_TIMEOUT', msg='gateway.4009: session timed out'),

  '4010': AlertString(if_err_type='warn', name='INVALID_SHARD', msg='gateway.4010: bad shard for bot'),

  '4011': AlertString(if_err_type='warn', name='SHARDING_NEEDED', msg='gateway.4011: sharding is required')
}

