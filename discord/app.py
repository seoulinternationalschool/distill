import requests
import json
from .threads import ThreadPoolHandler
from .network import Requester
from .channels import TextChannels, VoiceChannels
from .accounts import *
from .guilds import GuildUtil
from .dm import DMUtil
from .data import *
from .gateway.gateway import GatewayUtil

class App:
  def __init__(self, config):
    self.config = config

    self.baseURL = constants.BASE_URL

    self.thread_pool = ThreadPoolHandler()

    # init a http class attribute for App
    self.http = Requester(self)

    # when token is recieved, attach as attribute
    # TODO: cache token to reduce calls to /auth/login
    ret = self.http.api_login(self.config['id'], self.config['passwd'])

    if 'token' in ret:
      self.token = ret['token']
    else:
      print(ret)
      return

    # accounts class for others profiles, modifying settings
    self.utils = Utils(self)

    self.text = TextChannels(self)

    #guilds class for everything about servers
    self.guilds = GuildUtil(self)

    # dm class
    self.dm = DMUtil(self)


    #TODO
    #self.channels.voice = ()

    # profile
    self.profile = Profile(self)

    self.gateway = GatewayUtil(self)
