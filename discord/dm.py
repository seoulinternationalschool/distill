from .data import constants

class DMUtil:
    def __init__(self, app):
      self.app = app
    
    
    def get_all(self):
      all_dm = (self.app.http.request('get', constants.ROUTES['MY_DM'], True))
      return [self.app.text.Channel(self.app, x) for x in all_dm]
    def create(self, id):
      payload = {
        'recipient_id': id
      }

      return (self.app.http.request('post', constants.ROUTES['MY_DM'], True, json=payload))