import json, ssl, random, asyncio, socket, sys, struct
import pyaudio, websockets
import nacl.secret
from ..voice import opcodes
from .opus import OpusController
from .audiostream import AudioStreamPlayer
from termcolor import cprint


class DiscordVoiceSocket:
  def __init__(self, app, endpoint, **kwargs):
    # attach Discord.App
    self.app = app

    # save asyncio loop
    self.loop = asyncio.new_event_loop()
    asyncio.set_event_loop(self.loop)

    # identification information
    self.s_id = str(kwargs['server_id'])
    self.sess_id = kwargs['session_id']
    self.user_id = kwargs['user_id']
    self.token = kwargs['token']

    #using v3
    endpoint = endpoint + '?v=3'

    # manually append wss id
    if 'wss://' not in endpoint:
      endpoint = 'wss://' + endpoint

    self.endpoint = endpoint

  def start(self):

    # add to ThreadPoolHandler of Discord.App 
    self.app.thread_pool.add_thread(self.start_loop)


  def start_loop(self):

    # start asyncio event loop for ws connection
    self.loop.run_until_complete(self.connect())
    # self.loop.close()

  async def connect(self):
    #save endpoint just because
    ws_url = self.endpoint
    ssl_c = ssl.SSLContext()
    

    ws_url = ws_url.replace(':80', '')

    #establish connection
    self.ws_connection = await websockets.client.connect(ws_url, ssl=ssl_c)

    #using opcode 0: identify for voice servers

    await self._identify()
    await self._send_heartbeat()

    #using async iteration
    async for message in self.ws_connection:
      message = json.loads(message)
      # print('VOICE_WEB: ' + json.dumps(message, indent=2))
      await self.message_handle(message)

  #used to send identify
  async def _identify(self):
    # print('identifying')
    await self.ws_connection.send(opcodes.opcode_0(self.s_id, self.sess_id, self.user_id, self.token))

  #handlers for all websocket messages
  async def message_handle(self, msg):
    code = msg['op']
    content = msg['d']

    # opcode READY
    if code == 2:

      recieved_params = {
        'ssrc': content['ssrc'],
        'port': content['port'],
        'modes': content['modes'],
        'ip': content['ip']
      }

      # cprint('OPCODE 2 READY RECIEVED, IDENTIFICATION SUCCESSFUL', 'green')

      self.app.voice_udp_connection = DiscordVoiceUDP(self.app, **recieved_params)

    # opcode SESS_DESC
    elif code == 4:
      
      self.app.voice_udp_connection.init_voice_stream(content['secret_key'], content['media_session_id'])
    
    elif code == 5:
      if(content['speaking'] == True):
        self.app.voice_udp_connection.audio.define_ssrc(content['ssrc'])
      else:
        self.app.voice_udp_connection.audio.undefine_ssrc(content['ssrc'])

    #opcode HELLO
    elif code == 8:

      self.heartbeat_interval = (content['heartbeat_interval'] / 1000) * 0.75 #Discord bug of hb interval

      asyncio.ensure_future(self.heart_beater(), loop = self.loop)
        
    
  #keeps heartbeating 
  async def heart_beater(self):
    while True:
      await self._send_heartbeat()
      await asyncio.sleep(self.heartbeat_interval)

  #send opcode 3
  async def _send_heartbeat(self):
    # cprint('heartbeat sent', 'green')
    await self.ws_connection.send(opcodes.opcode_3(self.nonce()))

  async def _send_protocol(self, ip, port):
    # cprint('OK: VOICE PROTOCOL SENT', 'green')
    await self.ws_connection.send(opcodes.opcode_1(ip, port))

  async def _send_speaking(self, ssrc):
    # cprint('OK: OPCODE 5, VOICE CONNECTED', 'green')
    await self.ws_connection.send(opcodes.opcode_5(ssrc))

  #produce nonce for opcode 3's 'd' field
  def nonce(self):
    return random.randint(0, 100000000)
    


class DiscordVoiceUDP:

  TYPE_HEADER = 0x80
  VERSION_HEADER = 0x78

  def __init__(self, app, **conn_params):
    
    self.ip = conn_params['ip']
    self.port = conn_params['port']
    self.ssrc = conn_params['ssrc']
    self.supported_modes = conn_params['modes']
    self.app = app

    #init rtp standards
    self.timestamp = random.randint(0, (2**8)**4)
    self.sequence = 0

    #for ip discovery
    self._got_first = False

     # using UDP DGRAM
    self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.udp_socket.bind(('0.0.0.0', self.port))
    # cprint('UDP connection initiated @:\n{}\n{}\n{}'.format(self.ip, self.port, self.ssrc), 'blue')

    # audio I/O
    self.audio = AudioStreamPlayer()

    # audio decode/encode
    self.opus = OpusController(AudioStreamPlayer.RATE, AudioStreamPlayer.CHANNELS, 'voip')


    # get external ip, port
    self.ip_discovery()

    # <thread 3>
    self.app.thread_pool.add_thread(self._socket_reciver_thread)
  # send a message to the set endpoint
  def _send(self, raw):
    self.udp_socket.sendto(raw, (self.ip, self.port))
  
  # loop
  def recieve(self):
    while True:
      yield self.udp_socket.recvfrom(1024)

  # used to find obfuscated UDP port and ip
  def ip_discovery(self):
    packet = self._get_packet(bytearray(58))

    self._send(packet)

  def _voice_sender_thread(self):
    
    for packet in self.audio.stream_input.data_iteration():

      #encode data
      audio = self.opus.encode(packet, AudioStreamPlayer.CHUNK_SIZE)
      audio_packet = self._get_audio_packet(audio)
      self._send(audio_packet)
      
      
      if self.timestamp + AudioStreamPlayer.SAMPLES_INC > (2**8)**4:
        self.timestamp = 0
      else:
        self.timestamp = self.timestamp + AudioStreamPlayer.SAMPLES_INC

      if self.sequence + 1 > (2**8)**2:
        self.sequence = 0
      else:
        self.sequence += 1

  def _socket_reciver_thread(self):
    for packet in self.recieve():
    
      # ip discovery
      if not self._got_first:
        content, addr = packet        
        self._got_first = True
        self._do_ip_choice(content)
        continue

      # incoming voice audio
      self._decode_audio_packet(packet[0])
      

  def _do_ip_choice(self, raw):
    real_ip = (raw[4:20].decode('utf-8'))
    real_port = struct.unpack('<H', raw[-2:])[0]

    # cprint('IP Discovery successful:\nip: {}\nport: {}'.format(real_ip, real_port), color='magenta')

    asyncio.run_coroutine_threadsafe(self.app.voice._send_protocol(real_ip, real_port), self.app.voice.loop)

  def _get_packet(self, raw):
    data = raw
    header = bytearray(12)

    # RTP headers via Discord
    header[0] = DiscordVoiceUDP.TYPE_HEADER
    header[1] = DiscordVoiceUDP.VERSION_HEADER

    # other RTP header stuff
    struct.pack_into('>H', header, 2, self.sequence)
    struct.pack_into('>I', header, 4, self.timestamp)
    struct.pack_into('>I', header, 8, self.ssrc)

    return header + data

  def _get_audio_packet(self, raw):
    data = raw
    header = bytearray(12)
    nonce = bytearray(24)

    # RTP headers via Discord
    header[0] = DiscordVoiceUDP.TYPE_HEADER
    header[1] = DiscordVoiceUDP.VERSION_HEADER

    # other RTP header stuff
    struct.pack_into('>H', header, 2, self.sequence)
    struct.pack_into('>I', header, 4, self.timestamp)
    struct.pack_into('>I', header, 8, self.ssrc)

    # nonce 0-11 (first 12) = header
    nonce[:12] = header

    #enc
    return header + self.encrypter.encrypt(bytes(data), bytes(nonce)).ciphertext

  def _decode_audio_packet(self, raw):

    # bytearray for convinience
    raw = bytearray(raw)
    

    # first 12 bytes is the header, probably
    header = raw[:12]


    # chop the first part
    type_raw = header[0]
    

    # ver_raw = header[1]


    # # decode the rest
    # sequence = struct.unpack_from('>H', header, 2)
    # timestamp = struct.unpack_from('>I', header, 4)
    ssrc = struct.unpack_from('>I', header, 8)[0]
    
    # print('packet at {} {} {}'.format(sequence, timestamp, ssrc))

    #get data
    data = raw[12:]
    # use the nonce and our secret key
    nonce = bytearray(24)
    nonce[:12] = header

    #decode
    decrypted = nacl.secret.SecretBox(key=bytes(self._secret_key)).decrypt(bytes(data), nonce=bytes(nonce))
    
    #play
    if type_raw == 0x80:
      aud = self.opus.decode(decrypted, AudioStreamPlayer.CHUNK_SIZE)
    else:
      aud = self.opus.decode(decrypted[8:], AudioStreamPlayer.CHUNK_SIZE)

    self.audio.push(aud, ssrc)

  def init_voice_stream(self, secret, session):
    self._secret_key = secret 
    self._session = session
    self.encrypter = nacl.secret.SecretBox(key=bytes(self._secret_key))

    asyncio.run_coroutine_threadsafe(self.app.voice._send_speaking(self.ssrc), self.app.voice.loop)
    self.audio.init_sound()
    self.app.thread_pool.add_thread(self._voice_sender_thread)

    


    

