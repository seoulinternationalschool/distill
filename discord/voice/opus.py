import opuslib

class OpusController:

  def __init__(self, rate, channels, application):
    self.rate = rate
    self.channels = channels
    self.encoder = opuslib.Encoder(rate, channels, application)
    self.decoder = opuslib.Decoder(rate, channels)

  def encode(self, raw, frame):
    self._check_args(raw)
    return self.encoder.encode(raw, frame)

  def decode(self, raw, frame):
    self._check_args(raw)
    return self.decoder.decode(raw, frame)

  # is it bytes-like?
  def _check_args(self, raw):
    if type(raw) is not bytes:
      raise TypeError('Not a bytes-like argument')
