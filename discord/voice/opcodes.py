import json

def opcode_0(server_id, session_id, user_id, token):
  return json.dumps({
    'op': 0,
    'd': {
      'server_id': server_id,
      'user_id': user_id,
      'session_id': session_id,
      'token': token
    }
  })

def opcode_3(nonce):
  return json.dumps({
    'op': 3,
    'd': nonce
  })

def opcode_1(ip, port):
  return json.dumps({
    'op': 1,
    'd': {
      'protocol': 'udp',
      'data':{
        'address': ip,
        'port': port,
        'mode': 'xsalsa20_poly1305'
      }
    }
  })

def opcode_5(ssrc):
  return json.dumps({
    'op': 5,
    'd': {
      'speaking': False,
      'delay': 0,
      'ssrc': ssrc
    }
  })

