import asyncio, subprocess, os
from time import sleep
from pygame import mixer
from ..threads import ThreadPoolHandler
from functools import reduce
import pyaudio, opuslib

class AudioStreamPlayer:

  #stereo
  CHANNELS = 2

  #48k standard as called for by Discord
  RATE = 48000

  #frames per buffer
  CHUNK_SIZE = 960

  #increment for RTP timestamp
  SAMPLES_INC = int(RATE / 1000 * CHUNK_SIZE)

  def __init__(self):
    mixer.init(frequency=47900, size=-16, channels=2, buffer=32768)
    mixer.set_num_channels(10)


    self.voices = ThreadPoolHandler()
    self.buffers = {}
    self.occupied = 1
    self.chan = {}
    self.pipes = {}
    self.ssrc_list = []
    self.channel = mixer.Channel(1)

    # init audio interface
    self.audio = pyaudio.PyAudio()

    # audio stream for getting audio input AND outputting voice audio data
    self.read_stream = self.audio.open(format = pyaudio.paInt16, 
                                        channels = AudioStreamPlayer.CHANNELS,
                                        rate = AudioStreamPlayer.RATE,
                                        input = True,
                                        frames_per_buffer = AudioStreamPlayer.CHUNK_SIZE)

    
    #async iterator for the luls
    self.stream_input = self.ReadBytesIterator(self, self.read_stream)                                    
  
  def define_ssrc(self, ssrc):
    self.ssrc_list.append(ssrc)
    self.buffers[ssrc] = bytearray()
    
    self.chan[ssrc] = mixer.Channel(self.occupied)
    self.occupied = self.occupied + 1
    self.voices.add_thread(self.wait_on_buffer, ssrc)
  
  def undefine_ssrc(self, ssrc):
    self.ssrc_list.remove(ssrc)

    del self.buffers[ssrc]
    del self.chan[ssrc]

  def push(self, raw, ssrc):
      self.buffers[ssrc].extend(raw)

  def wait_on_buffer(self, ssrc):

    while True:
      if not self.chan[ssrc].get_queue():
        
        self.chan[ssrc].queue(mixer.Sound(self.buffers[ssrc][:192000*5]))
        self.buffers[ssrc] = self.buffers[ssrc][192000*5:]

      sleep(5)

  def init_sound(self):
    pass
    

  class ReadBytesIterator:

    # iterator for the bytes
    def __init__(self, player, stream):
      self.stream = stream
      self.player = player
      self.closed = False


    def data_iteration(self):

      while True:
        yield self.stream.read(AudioStreamPlayer.CHUNK_SIZE, exception_on_overflow=False)

        if(self.closed): break

      raise StopIteration('iteration of data stopped')
    
    def close_iteration(self):
      self.closed = True

    
      
