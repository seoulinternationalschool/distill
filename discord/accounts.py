#module will attach to app class
from .data import constants

class Account: 
  def __init__(self, user_data):
    self._set_data(user_data)

  def _set_data(self, dict):
  #set attributes for all profile keys
    for key in dict:

      # set as 'private' attribute
      setattr(self, "_" + key, dict[key])

class Profile(Account): 
  def __init__(self, app):
    self.app = app
    data = self._getMe()
    super().__init__(data)

  #get personal profile data
  def _getMe(self):
    print('getting profile data...')
    return (self.app.http.request('get', constants.ROUTES['MY_PROFILE'], True))
  
  def modify(self, usname, avatar):
    payload = {
      'username': usname
    }
    if avatar != None:
      payload['avatar'] = avatar

    return (self.app.http.request('patch', constants.ROUTES['MY_PROFILE'], True, json=payload))
    
  def get_messages(self, channel):
      return (self.app.http.request('get', constants.ROUTES['CH_MSG'] % channel, True))

  def send_message(self, channel, json):
      return (self.app.http.request('post', constants.ROUTES['CH_MSG'] % channel, True, json=json))

  def connections(self):
      return (self.app.http.request('get', constants.ROUTES['MY_PROFILE'] + '/connections', True))
      

class Utils: 
  def __init__(self, app):
    self.app = app

  #find a user based on client id
  def get_user(self, id):
    return self.app.http.request('post', constants.ROUTES['GET_USER'] + id, True)



  


  

  

  