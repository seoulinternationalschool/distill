from .data import constants

class TextChannels:
  def __init__(self, app):
    if not app:
      return
    self.app = app
  
  def get(self, channel_id):
    return self.Channel(self.app, channel_id)
  
  class Channel:
    def __init__(self, app, channel_obj):
      self.app = app
      self.channel_id = channel_obj['id']
      self.details = channel_obj
    
    def get_all_messages(self, d):
      return self.app.http.request('get', constants.ROUTES['CH_MSG'] % self.channel_id, True, params=d)

    def get_message(self, msg_id):
      return self.app.http.request('get', constants.ROUTES['CH_MSG'] % self.channel_id + '/%s' % msg_id, True)

    def delete_message(self, msg_id):
      return self.app.http.request('delete', constants.ROUTES['CH_MSG'] % self.channel_id + '/%s' % msg_id, True)

    def send_message(self, d):
      return self.app.http.request('post', constants.ROUTES['CH_MSG'] % self.channel_id, True, json=d)

    def modify(self, d):
      return self.app.http.request('patch', '/channels/%s' % self.channel_id, True, json=d)

    def delete(self):
      return self.app.http.request('patch', '/channels/%s' % self.channel_id, True)


class VoiceChannels:
  def __init__(self):
    pass
