from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from threading import Thread
import multiprocessing

multiprocessing.set_start_method('fork')

class ThreadPoolHandler:
  def __init__(self):
    self.threads = []
    self.processes = []
 #-- add threads to pool w/ callback --#
  def add_thread(self, fn, *fn_arg, **fn_kwargs):
    t = Thread(target=fn, args=fn_arg)
    self.threads.append(t)
    t.start()
    print(self.threads)

  def add_process(self, fn, *fn_arg, **fn_kwargs):
    p = multiprocessing.Process(target=fn, args=fn_arg)
    self.processes.append(p)
    p.start()
    print(self.processes)


  